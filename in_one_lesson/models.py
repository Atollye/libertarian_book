from django.db import models

from django.urls import reverse

class Chapter(models.Model):
    chapter_num = models.IntegerField()
    chapter_num_txt = models.CharField(max_length=150)
    chapter_title = models.CharField(max_length=150)
    chapter_text = models.TextField()
    image = models.ImageField(upload_to='images/', null=True)


    def __str__(self):
        return self.chapter_title

    def link_to_next_chapter(self):
        if self.chapter_num <15:
            self.num = self.chapter_num+1
        else:
            self.num = 1
        return reverse('chapter', args=[self.num])

# добавлено ради ссылки на предыдущую и последующие главы и "карусели" с превью
# случайныйх глав
    def link_to_previous_chapter(self):
        if self.chapter_num >1:
            self.num = self.chapter_num-1
        else:
            self.num = 15
        return reverse('chapter', args=[self.num])

    def get_absolute_url(self):
        return reverse('chapter', args=[self.chapter_num])




