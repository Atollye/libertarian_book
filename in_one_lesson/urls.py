from django.conf.urls import url

from in_one_lesson import views

urlpatterns = [
	url(r'^$',views.list_of_chapters, name ='index'),
	url(r'^index$',views.list_of_chapters, name ='index'),
	url(r'^chapters/(?P<key>\d+)/$', views.chapter, name ='chapter'),
	url(r'^about_bergland/$', views.about_bergland, name ='about_bergland'),
	url(r'^about_me/$', views.about_me, name ='about_me'),
   ]
