from django.shortcuts import render
from .models import Chapter

import random


def list_of_chapters(request):
	chapters = Chapter.objects.order_by('chapter_num')
	return render(request, 'in_one_lesson/index.html',{'chapters':chapters})

def chapter(request, key):
    q = int(key)
    lst = list(range(1, 17))
    if q!=17:
        lst.remove(q)
    samp = random.sample(lst,3)
    ch_one = Chapter.objects.get(chapter_num=samp[0])
    ch_two = Chapter.objects.get(chapter_num=samp[1])
    ch_three = Chapter.objects.get(chapter_num=samp[2])
    chapter = Chapter.objects.get(chapter_num=key)
    return render(request, 'in_one_lesson/chapter.html',
                  {'chapter': chapter,
                   'ch_one': ch_one,
                   'ch_two': ch_two,
                   'ch_three': ch_three,
                   } )

def about_bergland(request):
    return render(request, 'in_one_lesson/about_bergland.html')

def about_me(request):
	return render(request, 'in_one_lesson/about_me.html')







