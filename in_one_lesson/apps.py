from django.apps import AppConfig


class InOneLessonConfig(AppConfig):
    name = 'in_one_lesson'
