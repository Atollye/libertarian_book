from django.contrib import admin

from .models import Chapter



class Chapter_List_Admin(admin.ModelAdmin):
	list_display = ["chapter_num","chapter_num_txt","chapter_title"]
	list_display_links = ["chapter_num_txt"]
	list_editable = ["chapter_title"]

	class Meta:
		model = Chapter


admin.site.register(Chapter,Chapter_List_Admin)
