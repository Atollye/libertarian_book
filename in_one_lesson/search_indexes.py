from haystack import indexes

from .models import Chapter

class ChapterIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
   # title = indexes.CharField(model_attr='chapter_title')
   # body = indexes.CharField(model_attr='chapter_text')

   #автокомплит
   #content_auto = indexes.EdgeNgramField(model_attr='chapter_text')

    def get_model(self):
        return Chapter #обращение к глобальной переменной изнутри класса
        #нужно, чтобы можно было заменит модель для тестирования

    def index_queryset(self, using=None):
        """используется, когда индекс обновляется целиком"""
        return self.get_model().objects.all()

