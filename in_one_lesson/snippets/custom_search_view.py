"""

#вьюха поиска
from .forms import SearchForm
#from haystack.forms import SearchForm
from haystack.query import SearchQuerySet

def SearchView(request):
    form = SearchForm()
    if 'q' in request.GET:
        all_results = SearchQuerySet().all()
    else:
        all_results = [1,2,3,4]
    return render( request,
                   'search/search.html',
                  { 'form': form,
                    'results': all_results,
                       } )

"""