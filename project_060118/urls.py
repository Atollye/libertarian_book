"""project_060118 URL Configuration"""

from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings


urlpatterns = [
    url(r'', include('in_one_lesson.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^search/', include('haystack.urls')),
    ]






