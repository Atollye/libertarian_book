"""
WSGI config for project_060118 project.
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "project_060118.settings")

application = get_wsgi_application()
